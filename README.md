# Fan control

Control fan speed via GPIO pins by pulse width modulation.

## Usage
```bash
$ fan-control --help
Usage: fan-control [OPTIONS]

Options:
      --pin-number <pin-number>
          Pin number for the GPIO pin which controls the fan [default: 21]
      --frequency <frequency>
          Frequency in Hertz of the Pwm duty cycle [default: 100]
      --temperature-path <temperature-path>
          Path to the sysfs temperature file. The default is that of a raspberry pi model 4 [default: /sys/class/thermal/thermal_zone0/temp]
      --gpio-chip <gpio-chip>
          Device with which to control the GPIO pins (chardev) [default: /dev/gpiochip0]
      --min-temperature <min-temperature>
          Turn fan on when temperature reaches this level [default: 40]
      --max-temperature <max-temperature>
          Turn fan on when temperature reaches this level [default: 70]
      --temperature-check-interval <check-interval>
          Temperature check interval [default: 1]
  -d, --debug
          Enable debug output
  -h, --help
          Print help
```

## Development
If cross-compiling for a raspberry pi:
- Download the latest aarch64 toolchain from
  <https://toolchains.bootlin.com/downloads/releases/toolchains/aarch64/tarballs/>
- Add to your .cargo/config.toml or equivalent:
```
[target.aarch64-unknown-linux-musl]
linker = "/toolchains/aarch64--musl--stable-2024.05-1/bin/aarch64-buildroot-linux-musl-gcc"
rustflags = [
  "-C", "link-arg=-lgcc",
  "-C", "target-feature=+crt-static",
]
```
- Then build:
```
$ rustup target add aarch64-unknown-linux-musl
$ cargo build --release --target aarch64-unknown-linux-musl
```
