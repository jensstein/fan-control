use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use std::fs;

use clap::Parser;
use gpio_cdev::{Chip, Line, LineRequestFlags};

#[derive(Parser)]
struct Args {
    /// Pin number for the GPIO pin which controls the fan.
    #[arg(long, value_name = "pin-number", default_value_t = 21)]
    pin_number: u32,
    /// Frequency in Hertz of the Pwm duty cycle
    #[arg(long, value_name = "frequency", default_value_t = 100)]
    frequency: u16,
    /// Path to the sysfs temperature file. The default is that of a raspberry pi model 4.
    #[arg(long, value_name = "temperature-path", default_value = "/sys/class/thermal/thermal_zone0/temp")]
    temperature_path: String,
    /// Device with which to control the GPIO pins (chardev).
    #[arg(long, value_name = "gpio-chip", default_value = "/dev/gpiochip0")]
    gpio_chip: String,
    /// Turn fan on when temperature reaches this level
    #[arg(long, value_name = "min-temperature", default_value_t = 40)]
    min_temperature: u16,
    /// Turn fan on when temperature reaches this level
    #[arg(long, value_name = "max-temperature", default_value_t = 70)]
    max_temperature: u16,
    /// Temperature check interval
    #[arg(long, value_name = "check-interval", default_value_t = 1)]
    temperature_check_interval: u64,
    /// Enable debug output
    #[arg(short, long)]
    debug: bool,
}

#[derive(Debug, Clone)]
pub struct Error {
    message: String,
}
impl Error {
    pub fn new(message: String) -> Self {
        Self {message}
    }
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for Error {}

impl From<gpio_cdev::errors::Error> for Error {
    fn from(error: gpio_cdev::errors::Error) -> Self {
        Self::new(format!("GPIO error: {error}"))
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self::new(format!("IO error: {error}"))
    }
}

#[derive(Debug)]
struct Pwm {
    gpio_line: Line,
    frequency: u16,
    activity_on: f32,
    activity_off: f32,
    rx: mpsc::Receiver<u16>,
}

type FanResult<V> = Result<V, Error>;

impl Pwm {
    fn start(&mut self) -> FanResult<()> {
        let output_handle = self.gpio_line.request(LineRequestFlags::OUTPUT, 0, "fan-control")?;
        loop {
            let activity_on = unsafe { (self.activity_on * 1000.0).to_int_unchecked::<u64>() };
            let activity_off = unsafe { (self.activity_off * 1000.0).to_int_unchecked::<u64>() };
            output_handle.set_value(1)?;
            thread::sleep(Duration::from_millis(activity_on));
            output_handle.set_value(0)?;
            thread::sleep(Duration::from_millis(activity_off));
            if let Ok(cycle) = self.rx.try_recv() {
                self.set_cycle(cycle);
            }
        }
    }

    fn set_cycle(&mut self, duty_cycle: u16) {
        // The duty cycle is specified in percentages so convert to a decimal
        let proportion: f32 = duty_cycle as f32 / 100.0;
        // Frequency is specified in Hz and since the python time.sleep
        // method takes input in seconds, we divide by one to get the total
        // amount of time to be used by a cycle.
        let interval: f32 = 1.0 / self.frequency as f32;
        self.activity_on = proportion * interval;
        self.activity_off = interval - self.activity_on;
    }
}

fn linear_conversion(temperature: u16, min_temp: u16, max_temp: u16) -> u16 {
    // This converts a temperature reading in the scale between 40 and 70 to a value between 0 and 100
    // https://stackoverflow.com/a/929107
    if temperature < min_temp {
        return 0;
    } else if temperature > max_temp {
        return 100;
    }
    (temperature - min_temp) * 100 / (max_temp - min_temp)
}

fn read_temperature(temperature_path: &str) -> FanResult<u16> {
    let c = fs::read_to_string(temperature_path)?;
    match (c.trim()).parse::<u16>() {
        Ok(val) => Ok(val / 1000),
        Err(_error) => Err(Error::new("Unable to parse temperature {c}".into()))
    }
}

fn main() {
    let args = Args::parse();
    let mut log_builder = env_logger::Builder::from_default_env();
    log_builder.filter(None, log::LevelFilter::Info);
    if args.debug {
        log_builder.filter(None, log::LevelFilter::Debug);
    }
    log_builder.init();

    let (tx, rx) = mpsc::channel();

    let mut chip = match Chip::new(&args.gpio_chip) {
        Ok(chip) => chip,
        Err(error) => {
            log::info!("Unable to aquire GPIO connection on {}: {}", args.gpio_chip, error);
            std::process::exit(1);
        }
    };
    let handle = match chip
            .get_line(args.pin_number) {
        Ok(handle) => handle,
        Err(error) => {
            log::info!("Unable to aquire handle on GPIO pin {}: {}", args.pin_number, error);
            std::process::exit(1);
        }
    };
    thread::spawn(move || {
        let mut pwm = Pwm {
            gpio_line: handle,
            frequency: args.frequency,
            activity_on: 0.0,
            activity_off: 0.0,
            rx
        };
        if let Err(error) = pwm.start() {
            log::info!("Unable to start Pwm process {error}");
            std::process::exit(1);
        }
    });

    loop {
        let temperature = match read_temperature(&args.temperature_path) {
            Ok(t) => t,
            Err(error) => {
                log::info!("Unable to read temperature from {}: {}", args.temperature_path, error);
                std::process::exit(1);
            }
        };
        log::debug!("Temperature: {temperature}");
        let cycle = linear_conversion(temperature, args.min_temperature,
            args.max_temperature);
        if let Err(error) = tx.send(cycle) {
            log::info!("The thread controlling the fan disappeared!");
            log::debug!("The error was {error}");
            std::process::exit(1);
        }
        thread::sleep(Duration::from_secs(args.temperature_check_interval));
    }
}
